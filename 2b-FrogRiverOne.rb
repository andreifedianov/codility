# Solution should be in O(n)

def solution(x, a)
	result = Array.new(x + 1)
	result.fill(0)
	result[0] = 9
	for i in 0 .. a.length - 1 do
		if a[i] <= x
			result[a[i]] = result[a[i]] + 1
		end
		if !(result.include? 0)
			return i
		end
	end
	return -1
end

def run_this_thing
	x = 5
	a = [1, 3, 1, 4, 2, 3, 5, 4]
	puts "A=" + a.to_s + "\tSolution=" + solution(x,a).to_s

	x = 5
	a = [1, 3, 1, 4, 2, 3, 3, 4]
	puts "A=" + a.to_s + "\tSolution=" + solution(x,a).to_s

	x = 5
	a = [1, 3, 1, 4, 3, 3, 3, 5,4,3,2,5]
	puts "A=" + a.to_s + "\tSolution=" + solution(x,a).to_s + " minute(s)"
end


run_this_thing