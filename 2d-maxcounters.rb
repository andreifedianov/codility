def solution(n, a)
	result = Array.new(n)
	result.fill(0)
	maxValue = 0

	for i in 0 .. a.length - 1 do
		if a[i] <= n
			result[a[i] - 1] = result[a[i] - 1].to_i + 1
			if maxValue < result[a[i] - 1]
				maxValue = result[a[i] - 1]
			end
		else
			result.fill(maxValue)
		end
	end
	return result
end

def run_this_thing
	n = 5
	a = [3, 4, 4, 6, 1, 4, 4]
	puts "Solution=" + solution(n,a).to_s



	n = 5
	a = [3, 6, 4, 4, 1, 6, 4, 5]
	puts "Solution=" + solution(n,a).to_s
end


run_this_thing