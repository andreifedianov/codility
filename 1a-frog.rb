
def solution(x, y, d)
	i = x
	count = 0
	while i < y do
		i += d

		count += 1
	end
	return count
end

def run_this_thing
	x = 10
	y = 85
	d = 30
	puts "Solution=" + solution(x,y,d).to_s
	x = 10
	y = 85
	d = 40
	puts "Solution=" + solution(x,y,d).to_s
end


run_this_thing