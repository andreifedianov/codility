def solution(a)
	n = a.length
    count = Array.new(n + 1)
    count.fill(0)
	for i in 0 .. n - 1 do
		count[a[i]] = 1
	end
	for i in 1 .. n + 1 do
		if(count[i] == 0)
			return i
		end
	end
	return (n + 1)
end

def run_this_thing
	# a = [2,3,1,5]
	# puts "Solution=" + solution(a).to_s


	# a = [2,3,1,5,4,7]
	# puts "Solution=" + solution(a).to_s


	# a = [2]
	# puts "Solution=" + solution(a).to_s

	a = [2,3,4,5]
	puts "Solution=" + solution(a).to_s

	a = [1,2,3]
	puts "Solution=" + solution(a).to_s
end


run_this_thing