# Array A consisting of N integers
# 0 < P < N
# Any integer P, such that 0 < P < N, splits this tape into two non-empty parts: A[0], A[1], ..., A[P − 1] and A[P], A[P + 1], ..., A[N − 1]


def solution(a)
    if(a.length < 2)
        return 0
    end
    result = -1
    for p in 0 .. a.length
        newresult = getP(a, p)
        if(newresult < result || result < 0)
            result = newresult
        end
    end
    return result
end

def getP(a, p)
    pleft = 0
    pright = 0
    for i in 0 .. a.length - 1
        if(i < p)
            pleft += a[i]
        else
            pright += a[i]
        end
    end
    return (pleft - pright).abs
end

def run_this_thing
    a = [3,1,2,4,3]
    puts "Solution= " + solution(a).to_s

    a = [-3,-1,-2,-4,-3]
    puts "Solution= " + solution(a).to_s

end

def solution(ar)
  sum = ar.inject(:+)
  left = ar[0]
  right = sum - left
  min_diff = (right - left).abs

  1.upto(ar.size - 2) do |i|
    left += ar[i]
    right -= ar[i]
    diff = (right - left).abs
    min_diff = [min_diff, diff].min
  end

  # Result.
  return min_diff
end

#--------------------------------------- Tests

def test
  sets = []
  #sets << ["1", 1, [1]]
  sets << ["31", 2, [3, 1]]
  sets << ["312", 0, [3, 1, 2]]
  sets << ["[1]*4", 0, [1]*4]
  sets << ["[1]*5", 1, [1]*5]
  sets << ["sample", 1, [3, 1, 2, 4, 3]]

  sets.each do |name, expected, ar|
    out = solution(ar)
    puts out.to_s
    raise "FAILURE at test #{name.inspect}: #{out.inspect} != #{expected.inspect}" if out != expected
  end

  puts "SUCCESS: All tests passed"
end

test