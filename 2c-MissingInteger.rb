# Solution should be in O(n)

def solution(a)
	result = Array.new(a.sort[-1] + 1)
	result.fill(0)
	result[0] = 1
	for i in 0 .. a.length - 1 do
		if a[i] > 0
			result[a[i]] = result[a[i]] + 1
		end
	end
	if result.index(0).nil?
		return -1
	else
		return result.index(0)
	end
end

def run_this_thing
	a = [1, 3, 6, 4, 1, 2]
	puts "A=" + a.to_s + "\tSolution=" + solution(a).to_s
	
	a = [3]
	puts "A=" + a.to_s + "\tSolution=" + solution(a).to_s

	a = [1, 3, 6, 4, 1, 5, 3, 7, 9, 10, 8]
	puts "A=" + a.to_s + "\tSolution=" + solution(a).to_s

	a = [1, 3, 6, 4, 1, 5, 3, 7, 9, 10]
	puts "A=" + a.to_s + "\tSolution=" + solution(a).to_s

	a = [-5, -3, -1, 0, 3, 6]
	puts "A=" + a.to_s + "\tSolution=" + solution(a).to_s
end


run_this_thing