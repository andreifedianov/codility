# Array A consisting of N integers
# 0 < P < N
# Any integer P, such that 0 < P < N, splits this tape into two non-empty parts: A[0], A[1], ..., A[P − 1] and A[P], A[P + 1], ..., A[N − 1]


def solution(a)
    if(a.length < 2)
        return 0
    end

    result = -1
    for p in 0 .. a.length
        newresult = (getPLeft(a,p) - getPRight(a, p)).abs
        if(newresult < result || result < 0)
            result = newresult
        end
    end
    return result
end


def getPLeft(a, p)
    result = 0
    for i in 0 .. p - 1 do
        result += a[i]
    end
    return result
end

def getPRight(a, p)
    n = a.length - 1 
    result = 0
    for i in p .. n
        result += a[i]
    end
    return result
end

def run_this_thing()
    a = [3,1,2,4,3]
    puts "Solution= " + solution(a).to_s

    a = [1]
    puts "Solution= " + solution(a).to_s

end
run_this_thing