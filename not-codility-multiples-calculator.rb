#Sample: If we list all the natural number below 10 that are multiple of 3 or 5 we get 3,5,6,9, and the sum of these multiples is 23

#Question: Find the sum of all multiples of 3 OR 5 below 1000

def solution(input)
	result = 0
	for i in 0 .. input - 1
		if (i % 3) == 0
			result = result.to_i + i
		elsif (i % 5) == 0
			result = result.to_i + i
		end
		
	end
	return result
end

def run_this_thing
	a = 1000
	puts solution(a).to_s
end

run_this_thing