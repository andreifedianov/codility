
# Solution should be in O(n)

def solution(a)
	result = Array.new(a.length + 1)
	result.fill(0)
	result[0] = -1
	for i in 0 .. a.length - 1 do
		result[a[i]] = 1
	end
	return (!result.include?(0)) == true ? 1 : 0
end

def run_this_thing
	a = [4, 1, 3, 2] 
	puts "A=" + a.to_s + "\tSolution=" + solution(a).to_s

	a = [4, 1, 3] 
	puts "A=" + a.to_s + "\tSolution=" + solution(a).to_s
end


run_this_thing